<?php

/**
 * Properties editor class.
 */
class sweaver_plugin_editor extends sweaver_plugin {

  /**
   * Menu registry.
   */
  function menu_registry(&$weight) {
    $items = array();

    $base = array(
      'load arguments' => array('%map'),
      'access arguments' => array('configure sweaver'),
      'file' => 'plugins/sweaver_plugin_editor/sweaver_plugin_editor.admin.inc',
      'type' => MENU_CALLBACK,
    );

    $items['admin/settings/sweaver'] = array(
      'title' => 'Sweaver',
      'description' => 'Visual interface for tweaking or building Drupal themes.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('sweaver_settings'),
      'access arguments' => array('configure sweaver'),
      'file' => 'plugins/sweaver_plugin_editor/sweaver_plugin_editor.admin.inc',
    );
    $items['admin/settings/sweaver/settings'] = array(
      'title' => 'Settings',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => $weight++,
    );

    // Enable or disable plugins.
    $items['admin/settings/sweaver/plugins'] = $base + array(
      'title' => 'Plugins',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('sweaver_plugin_editor_config_plugins'),
      'weight' => $weight++,
    );
    $items['admin/settings/sweaver/plugins']['type'] = MENU_LOCAL_TASK;

    $menu_items = array(
      'selectors' => array(
        'title' => 'Selectors',
        'ctools_collection' => 'selectors',
        'ctools_object' => 'selector',
        'ctools_table' => 'sweaver_selector',
      ),
      'properties' => array(
        'title' => 'Properties',
        'ctools_collection' => 'properties',
        'ctools_object' => 'property',
        'ctools_table' => 'sweaver_property',
      ),
      'types' => array(
        'title' => 'Types',
        'ctools_collection' => 'types',
        'ctools_object' => 'type',
        'ctools_table' => 'sweaver_type',
      ),
    );

    foreach ($menu_items as $key => $item) {

      $items['admin/settings/sweaver/'. $item['ctools_collection']] = array(
        'title' => $item['title'],
        'page callback' => 'sweaver_objects_list',
        'page arguments' => array($item['ctools_object']),
        'access arguments' => array('configure sweaver'),
        'file' => 'plugins/sweaver_plugin_editor/sweaver_plugin_editor.admin.inc',
        'type' => MENU_LOCAL_TASK,
        'weight' => $weight++,
      );
      $items['admin/settings/sweaver/add/'. $item['ctools_object']] = $base + array(
        'title' => 'Add new '. $item['ctools_object'],
        'page callback' => 'drupal_get_form',
        'page arguments' => array('sweaver_object_form', $item['ctools_object']),
      );
      $items['admin/settings/sweaver/edit/'. $item['ctools_object'] .'/%sweaver_object'] = $base + array(
        'title' => 'Edit '. $item['ctools_object'],
        'page callback' => 'drupal_get_form',
        'page arguments' => array('sweaver_object_form', $item['ctools_object'], 5),
      );
      $items['admin/settings/sweaver/delete/'. $item['ctools_object'] .'/%sweaver_object'] = $base + array(
        'title' => 'Delete '. $item['ctools_object'],
        'page callback' => 'drupal_get_form',
        'page arguments' => array('sweaver_object_delete', $item['ctools_collection'], $item['ctools_object'], 5, 'delete', 'deleted'),
      );
      $items['admin/settings/sweaver/revert/'. $item['ctools_object'] .'/%sweaver_object'] = $base + array(
        'title' => 'Revert '. $item['ctools_object'],
        'page callback' => 'drupal_get_form',
        'page arguments' => array('sweaver_object_delete', $item['ctools_collection'], $item['ctools_object'], 5, 'revert', 'reverted'),
      );
      $items['admin/settings/sweaver/enable/'. $item['ctools_object'] .'/%sweaver_object'] = $base + array(
        'title' => 'Enable '. $item['ctools_object'],
        'page callback' => 'sweaver_object_status',
        'page arguments' => array($item['ctools_collection'], 5, FALSE),
      );
      $items['admin/settings/sweaver/disable/'. $item['ctools_object'] .'/%sweaver_object'] = $base + array(
        'title' => 'Disable '. $item['ctools_object'],
        'page callback' => 'sweaver_object_status',
        'page arguments' => array($item['ctools_collection'], 5, TRUE),
      );
      $items['admin/settings/sweaver/export/'. $item['ctools_object'] .'/%sweaver_object'] = $base + array(
        'title' => 'Export '. $item['ctools_object'],
        'page callback' => 'sweaver_object_export',
        'page arguments' => array($item['ctools_object'], 5),
      );
    }

    // Editor form configuration.
    $items['admin/settings/sweaver/editor'] = $base + array(
      'title' => 'Editor',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('sweaver_plugin_editor_config_editor'),
      'weight' => $weight++,
    );
    $items['admin/settings/sweaver/editor']['type'] = MENU_LOCAL_TASK;

    return $items;
  }

  /**
   * Theme registry.
   */
  function theme_registry() {
    $theme_functions = array();

    $editor_plugin_path = drupal_get_path('module', 'sweaver') .'/plugins/sweaver_plugin_editor';
    $theme_functions = array(
      'sweaver_plugin_editor_config_editor' => array(
        'template' => 'sweaver-plugin-editor-config-editor',
        'file' => 'sweaver_plugin_editor.theme.inc',
        'path' => $editor_plugin_path,
        'arguments' => array('form' => NULL),
      ),
      'sweaver_plugin_editor_config_plugins' => array(
        'template' => 'sweaver-plugin-editor-config-plugins',
        'file' => 'sweaver_plugin_editor.theme.inc',
        'path' => $editor_plugin_path,
        'arguments' => array('form' => NULL),
      ),
    );

    return $theme_functions;
  }

  /**
   * Frontend form.
   */
  function frontend_form() {
    $form = array();
    $form['#editor_containers'];

    $properties = sweaver_object_load(NULL, 'property');
    $sweaver_editor_form_configuration = variable_get('sweaver_editor_form_configuration',array());

    // Switcher.
    $tab_switcher = '<div class="editor-switcher"><span class="swichter-link"><a href="javascript:;">'. t('Style') .'</a></span><span class="swichter-link"><a href="javascript:;">'. t('History') .'</a></span></div>';
    $form['editor_switcher'] = array(
      '#type' => 'markup',
      '#value' => $tab_switcher,
    );

    // Images.
    $imagecache_presets = array();
    if (variable_get('sweaver_plugin_status_sweaver_plugin_imagecache', TRUE) && variable_get('sweaver_plugin_imagecache_alter_images', FALSE) && function_exists('imagecache_presets')) {
      $imagecache_presets = imagecache_presets();
    }

    $images = array('' => t('Select image'));
    $query = 'SELECT ti.fid, ti.description, f.filepath FROM {sweaver_image} ti
              INNER JOIN {files} f on f.fid = ti.fid order by description ASC';
    $result = db_query($query);
    while ($image = db_fetch_object($result)) {
      $images[base_path() . $image->filepath] = $image->description;

      // Imagecache presets.
      foreach ($imagecache_presets as $preset_key => $preset) {
        $images[base_path() . file_directory_path() .'/imagecache/'. $preset_key .'/'. $image->filepath] = $image->description .' ('. $preset['presetname'] .')';
      }
    }

    // Container and properties.
    foreach ($sweaver_editor_form_configuration as $container => $settings) {
      if (!empty($settings['properties'])) {
        // Container title.
        $form['#editor_containers'][$container]['title'] = check_plain($settings['title']);
        foreach ($settings['properties'] as $weight => $property_key) {
          $form['#editor_containers'][$container]['properties'][$property_key] = $property_key;
          // Create property element.
          $property = $properties[$property_key];
          sweaver_export_check_serialized_keys($property);
          $form[$property->name] = $this->sweaver_property_element($property, $images);

          // Any children?
          if ($property->property_type == 'parent') {
            foreach ($properties as $key => $prop) {
              if ($prop->property_parent == $property->name && !$prop->disabled)
              $form[$property->name][$prop->name] = $this->sweaver_property_element($prop, $images);
            }
          }
        }
      }
    }

    // History.
    $form['editor_history'] = array(
      '#type' => 'markup',
      '#value' => '<div id="editor-history"><h2>History</h2><div id="editor-history-list"></div></div>',
    );

    return $form;
  }

  /**
   * Frontend form render.
   */
  function frontend_form_render(&$vars, &$form, $plugin) {

    $name = $plugin['name'];
    $vars['tabs'][$name]['#tab_name'] = $form[$name]['#tab_name'];
    $vars['tabs_data'][$name]['#tab_description'] = $form[$name]['#tab_description'];

    // Width class.
    $container_class = 'container-'. count($form[$name]['form']['#editor_containers']);

    $output = '';

    // Tab switcher.
    $output .= drupal_render($form[$name]['form']['editor_switcher']);

    // Containers.
    $output .= '<div id="editor-styler">';
    foreach ($form[$name]['form']['#editor_containers'] as $key => $container_value) {
      $output .= '<div class="'. $container_class .' container"><div class="container-inner">';
      $output .= '<h2>'. $container_value['title'] .'</h2>';
      foreach ($container_value['properties'] as $key) {
        $output .= drupal_render($form[$name]['form'][$key]);
      }
      $output .= '</div></div>';
    }
    $output .= '</div>';

    // History
    $output .= drupal_render($form[$name]['form']['editor_history']);

    $vars['tabs_data'][$name]['content'] = $output;
  }

  /**
   * Frontend css and js.
   */
  function frontend_css_js(&$inline_settings) {

    drupal_add_css(drupal_get_path('module', 'sweaver') .'/plugins/sweaver_plugin_editor/sweaver_plugin_editor.css', 'module');
    drupal_add_js(drupal_get_path('module', 'sweaver') .'/plugins/sweaver_plugin_editor/sweaver_plugin_editor.js', 'module');

    // Serializer for database storage
    drupal_add_js(drupal_get_path('module', 'sweaver') .'/plugins/sweaver_plugin_editor/jquery.json-2.2.js', 'module');

    // Slider
    jquery_ui_add('ui.slider');
    // TODO: remove this when patch is added for automated inclusion of css
    drupal_add_css(drupal_get_path('module', 'jquery_ui') .'/jquery.ui/themes/default/ui.all.css', 'module');

    // colorpicker
    drupal_add_css(drupal_get_path('module', 'sweaver') .'/plugins/sweaver_plugin_editor/colorpicker/css/colorpicker.css');
    drupal_add_js(drupal_get_path('module', 'sweaver') .'/plugins/sweaver_plugin_editor/colorpicker/js/colorpicker.js', 'module');

    // Selectors.
    $js_selectors = array();
    $selectors = sweaver_object_load(NULL, 'selector');
    foreach ($selectors as $key => $selector) {
      $options = array(
        'description' => check_plain($selector->description),
        'selector' => check_plain($selector->selector_selector),
        'highlight' => $selector->selector_highlight,
      );
      $js_selectors[$key] = $options;
    }

    // Types.
    $js_types = array();
    $types = sweaver_object_load(NULL, 'type');
    foreach ($types as $key => $type) {
      sweaver_export_check_serialized_keys($type);
      $js_types[$key] = $type->type_options;
    }

    // Properties.
    $js_properties = array();
    $properties = sweaver_object_load(NULL, 'property');
    foreach ($properties as $key => $property) {
      sweaver_export_check_serialized_keys($property);
      $options = array(
        'name' => $property->name,
        'property' => $key,
        'parent' => $property->property_parent,
        'type' => $property->property_type,
        'options' => $property->property_options,
        'prefix' => $property->property_prefix,
        'suffix' => $property->property_suffix,
        'slider_min' => $property->property_slider_min,
        'slider_max' => $property->property_slider_max,
      );
      $js_properties[$key] = $options;
    }

    // Excluded selectors.
    $js_excluded_selectors = array();
    $exclude_selectors = variable_get('sweaver_selectors_exclude', SWEAVER_SELECTORS_EXCLUDE);
    $exploded = explode("\n", $exclude_selectors);
    foreach ($exploded as $key => $selector) {
      $trimmed = trim($selector);
      if (!empty($trimmed)) {
        $js_excluded_selectors[$trimmed] = $trimmed;
      }
    }

    $inline_settings['sweaver']['selectors'] = $js_selectors;
    $inline_settings['sweaver']['types'] = $js_types;
    $inline_settings['sweaver']['properties'] = $js_properties;
    $inline_settings['sweaver']['exclude_selectors'] = $js_excluded_selectors;
    $inline_settings['sweaver']['invokes'][] = 'sweaver_plugin_editor';
  }

  /**
   * Create a form element for a property.
   *
   * @param stdClass $property The property.
   * @param array $images A collection of images to use for the image type.
   */
  function sweaver_property_element($property, $images) {
    switch ($property->property_type) {

      case 'slider':
        return array(
          '#type' => 'textfield',
          '#title' => $property->description,
          '#attributes' => array(
            'class' => 'slider-value',
            'title' => $property->description,
          ),
        );
        break;

      case 'color':
        return array(
          '#type' => 'markup',
          '#value' =>  '<div id="edit-'. $property->name .'-wrapper" class="form-item clear-block">
            <label class="color">' . $property->description .':</label>
            <div id="'. $property->name .'" class="colorSelector-wrapper">
            <div class="colorSelector"><div style="background-color: #ffffff"></div>
            </div></div></div>'
        );
        break;

      case 'image':
        return array(
          '#type' => 'select',
          '#title' => $property->description,
          '#options' => $images,
          '#attributes' => array(
            'class' => 'background-image',
          ),
        );
        break;

      case 'select':
        return array(
          '#type' => 'select',
          '#title' => $property->description,
          '#options' => $property->property_options,
        );
        break;

      case 'parent':
        return array(
          '#type' => 'markup',
          '#prefix' => '<div class="sweaver-group clear-block"><label>'. $property->description .'</label><div class="sweaver-group-content">',
          '#suffix' => '</div></div>',
        );
        break;
    }
  }
}
