
-- SUMMARY --

The Sweaver module makes it possible to theme you website directly
on the front end.

For a full description of the module, visit the project page:
  http://drupal.org/project/sweaver
  
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/sweaver

-- REQUIRED --

CTools - http://drupal.org/project/ctools
Jquery UI - http://drupal.org/project/jquery_ui

-- MAINTAINERS --

swentel - http://drupal.org/user/107403
jyve - http://drupal.org/user/591438